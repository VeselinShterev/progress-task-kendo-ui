import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from '../../Models/post';
import { ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],

  encapsulation: ViewEncapsulation.None,

  styles: [
    `
  .k-grid.main {
    margin: 15px;
  }
  .k-grid.main tbody tr:hover {
    background: #CCECF9;
  }
  .k-pager-numbers .k-link {
    color: #96d0f2;
  }
  .k-grid td.k-state-selected, .k-grid td.k-selected, .k-grid tr.k-state-selected > td, .k-grid tr.k-selected > td {
    background-color: rgb(34 136 217 / 25%);
}
    `,
  ],
})

export class PostsComponent implements OnInit {
  readonly ROOT_URL = 'https://jsonplaceholder.typicode.com';

  public posts: Post[] = [];
  public checked = true;

  constructor(private http: HttpClient) { }

  getPosts(): void {
    this.http.get(this.ROOT_URL + '/posts')
      .subscribe(posts => {
        this.posts = posts as Post[]
      });
  }

  ngOnInit(): void {
    this.getPosts();
  }
}

