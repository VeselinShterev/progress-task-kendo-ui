# Veselin Kendo Angular Project Task


A Kendo Angular UI Grid based application

![image](./src/assets/app.images/image1.jpg)

### Used Technologies: Angular ClI, Kendo Angular UI

# Functionalities

* Kendo Angular Grid consuming remote data from external API
* Resizable Grid
* Paging 
* Sorting
* Kendo UI Toggle Switch to show/hide a certain grid column
* Grid styling (select & highlight rolls) and custom grid colors

# Installation
1. Clone the repository
2. npm install
3. ng serve --open


